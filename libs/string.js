String.prototype.replaceAll = function (search, replacement) {
    let target = this;
    return target.split(search).join(replacement);
};

String.prototype.trimStart = function (charlist) {
    let charval = charlist || " ";
    return this.replace(new RegExp("^[" + charval + "]+"), "");
};

String.prototype.trimEnd = function (charlist) {
    let charval = charlist || " ";
    return this.replace(new RegExp("[" + charval + "]+$"), "");
};

String.prototype.trim = function (charlist) {
    return this.trimStart(charlist).trimEnd(charlist);
};