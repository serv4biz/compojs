
class StackView {
    constructor(compoParent = null, elementId = "body", langCompo = compojs.globals["txt_language"]) {
        this.compoParent = compoParent;
        this.elementId = elementId;
        this.lang = langCompo;
        this.compos = [];
    };

    validate() {
        if (this.compos.length > 0) {
            let mythis = this;
            let lastItem = this.compos[this.compos.length - 1];

            $id(mythis.elementId).children().each(function (index) {
                let tagName = $(this)[0].nodeName.toLowerCase();
                if (tagName != "script" && tagName != "style") {
                    if ($(this).attr("compo") == lastItem.prefix) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                }
            });
        }
    };

    length() {
        return this.compos.length;
    };

    push(pathCompo, jsoParams = {}, lang = null) {
        let mythis = this;
        return new Promise(function (resolve, reject) {
            let comLang = mythis.lang;
            if (lang != null) {
                comLang = lang;
            }

            CompoJS.create(mythis.compoParent, pathCompo, jsoParams, comLang).then(function (compo) {
                compo.finalize(function (cp) {
                    for (let i = (mythis.compos.length - 1); i >= 0; i--) {
                        if (mythis.compos[i].id == cp.id) {
                            mythis.compos.splice(i, 1);
                            break;
                        }
                    }
                    mythis.validate();
                });

                mythis.compos.push(compo);
                $id(mythis.elementId).append(compo.render());
                mythis.validate();
                resolve(compo);
            }, reject);
        });
    };

    pop() {
        let item = this.compos.pop();
        if (item != undefined) {
            return item;
        }
        return null;
    };

    clear() {
        while (this.compos.length > 0) {
            this.compos.pop().delete();
        }
    }
};