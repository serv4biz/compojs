
URL.getPaths = function(pathUrl) {
  let sPageURL = pathUrl || window.location.pathname.substring(1);
  sPageURL = sPageURL.replaceAll(window.location.origin, "");
  sPageURL = sPageURL.trim("/");
  sPageURL = sPageURL.split('?')[0];
  let paths = sPageURL.split('/');
  let results = [];
  for (let pathname of paths) {
    if (pathname != "") {
      results.push(decodeURIComponent(pathname));
    }
  }
  return results;
};

URL.getParams = function(paramUrl) {
  let jsoData = {};
  let sPageURL = paramUrl || window.location.search.substring(1);
  sPageURL = sPageURL.replaceAll(window.location.origin, "");
  sPageURL = sPageURL.trim("/");
  sPageURL = sPageURL.trim("?");
  sPageURL = sPageURL.trim("&");
  if (sPageURL != "") {
    let sURLVariables = sPageURL.split('&');
    for(let parammap of sURLVariables) {
      let sParameterName = parammap.split('=');
      jsoData[decodeURIComponent(sParameterName[0])] = decodeURIComponent(sParameterName[1]);
    }
  }
  return jsoData;
};

URL.toParamsString = function(jsoParams) {
  jsoParams = jsoParams || URL.getParams();
  let txtURlString = "";
  for(let keyName in jsoParams) {
    txtURlString += encodeURIComponent(keyName);
    txtURlString += "=";
    txtURlString += encodeURIComponent(jsoParams[keyName]);
    txtURlString += "&";
  }
  return txtURlString.trim("&");
};