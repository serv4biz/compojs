class Ajax {
    static fetch(url) {
        if (window.fetch) {
            return new Promise(function (resolve, reject) {
                fetch(url).then(function (res) {
                    let headers = {};
                    for (let keyName of res.headers.keys()) {
                        headers[keyName] = res.headers.get(keyName).toLowerCase();
                    }

                    let results = {};
                    res.text().then(function (data) {
                        results.status = res.status;
                        results.statusText = HTTP.statusText(res.status);
                        if (results.statusText == "") {
                            results.statusText = res.statusText;
                        }
                        results.data = data;
                        results.headers = headers;

                        if (res.ok) {
                            results.ok = true;
                            compojs.globals["obj_cache"][url] = results;
                            compojs.storages.caches.setItem(url, JSON.stringify(results));
                            resolve(results);
                        } else {
                            results.ok = false;
                            reject(results);
                        }
                    });
                }, function (e) {
                    let results = {};
                    results.status = 0;
                    results.statusText = e.message;
                    results.data = "";
                    results.headers = {};
                    results.ok = false;
                    reject(results);
                });
            });
        } else {
            return new Promise(function (resolve, reject) {
                let xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        let headers = {};
                        let headerall = this.getAllResponseHeaders();
                        if (headerall != null && headerall != "") {
                            let lines = headerall.trim().split("\r\n");
                            for (let line of lines) {
                                let keymaps = line.split(": ");
                                headers[keymaps[0]] = keymaps[1];
                            }
                        }

                        let results = {};
                        results.status = xhttp.status;
                        results.statusText = HTTP.statusText(xhttp.status);
                        if (results.statusText == "") {
                            results.statusText = xhttp.statusText;
                        }
                        results.data = responseText;
                        results.headers = headers;

                        if (xhttp.status >= 200 && xhttp.status <= 299) {
                            results.ok = true;
                            compojs.globals["obj_cache"][url] = results;
                            compojs.storages.caches.setItem(url, JSON.stringify(results));
                            resolve(results);
                        } else {
                            results.ok = false;
                            results.statusText = "Error";
                            reject(results);
                        }
                    }
                };
                xhttp.open("GET", url, true);
                xhttp.send();
            });
        }
    };

    static get(url) {
        return new Promise(function (resolve, reject) {
            if (JSON.exist(compojs.globals["obj_cache"], url) && !compojs.configs["is_devmode"]) {
                let objItem = compojs.globals["obj_cache"][url];
                resolve(objItem);
            } else {
                if (compojs.configs["is_devmode"]) {
                    Ajax.fetch(url).then(resolve, reject);
                } else {
                    compojs.storages.caches.getItem(url).then(function (value) {
                        if (value == null) {
                            Ajax.fetch(url).then(resolve, reject);
                        } else {
                            let objItem = JSON.parse(value);
                            compojs.globals["obj_cache"][url] = objItem;
                            resolve(objItem);
                        }
                    });
                }
            }
        });
    };

    static requestRaw(url, rwdata = "", headers = {}) {
        if (window.fetch) {
            let myHeaders = new Headers();
            for (let keyName in headers) {
                myHeaders.append(keyName, headers[keyName]);
            }

            let requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: rwdata
            };

            return new Promise(function (resolve, reject) {
                fetch(url, requestOptions).then(function (res) {
                    let headers = {};
                    for (let keyName of res.headers.keys()) {
                        headers[keyName] = res.headers.get(keyName).toLowerCase();
                    }

                    let results = {};
                    res.text().then(function (data) {
                        results.status = res.status;
                        results.statusText = HTTP.statusText(res.status);
                        if (results.statusText == "") {
                            results.statusText = res.statusText;
                        }
                        results.data = data;
                        results.headers = headers;

                        if (res.ok) {
                            results.ok = true;
                            resolve(results);
                        } else {
                            results.ok = false;
                            reject(results);
                        }
                    });
                }, function (e) {
                    let results = {};
                    results.status = 0;
                    results.statusText = e.message;
                    results.data = "";
                    results.headers = {};
                    results.ok = false;
                    reject(results);
                });
            });
        } else {
            return new Promise(function (resolve, reject) {
                let xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        let headers = {};
                        let headerall = this.getAllResponseHeaders();
                        if (headerall != null && headerall != "") {
                            let lines = headerall.trim().split("\r\n");
                            for (let line of lines) {
                                let keymaps = line.split(": ");
                                headers[keymaps[0]] = keymaps[1];
                            }
                        }

                        let results = {};
                        results.status = xhr.status;
                        results.statusText = HTTP.statusText(xhr.status);
                        if (results.statusText == "") {
                            results.statusText = xhr.statusText;
                        }
                        results.data = responseText;
                        results.headers = headers;

                        if (xhr.status >= 200 && xhr.status <= 299) {
                            results.ok = true;
                            resolve(results);
                        } else {
                            results.ok = false;
                            results.statusText = "Error";
                            reject(results);
                        }
                    }
                };
                xhr.open("POST", url, true);
                for (let keyName in headers) {
                    xhr.setRequestHeader(keyName, headers[keyName]);
                }
                xhr.send(rwdata);
            });
        }
    };

    static request(url, params = {}, headers = {}) {
        return Ajax.requestRaw(url,URL.toParamsString(params),headers);
    };

    static postRaw(url, rwdata = "", headers = {}) {
        return Ajax.requestRaw(url, rwdata, headers);
    };

    static post(url, params = {}, headers = {}) {
        return Ajax.postRaw(url, URL.toParamsString(params), headers);
    };

    static upload(url, formdata = new FormData()) {
        if (window.fetch) {
            let requestOptions = {
                method: 'POST',
                body: formdata
            };

            return new Promise(function (resolve, reject) {
                fetch(url, requestOptions).then(function (res) {
                    let headers = {};
                    for (let keyName of res.headers.keys()) {
                        headers[keyName] = res.headers.get(keyName).toLowerCase();
                    }

                    let results = {};
                    res.text().then(function (data) {
                        results.status = res.status;
                        results.statusText = HTTP.statusText(res.status);
                        if (results.statusText == "") {
                            results.statusText = res.statusText;
                        }
                        results.data = data;
                        results.headers = headers;

                        if (res.ok) {
                            results.ok = true;
                            resolve(results);
                        } else {
                            results.ok = false;
                            reject(results);
                        }
                    });
                }, function (e) {
                    let results = {};
                    results.status = 0;
                    results.statusText = e.message;
                    results.data = "";
                    results.headers = {};
                    results.ok = false;
                    reject(results);
                });
            });
        } else {
            return new Promise(function (resolve, reject) {
                let xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (this.readyState == 4) {
                        let headers = {};
                        let headerall = this.getAllResponseHeaders();
                        if (headerall != null && headerall != "") {
                            let lines = headerall.trim().split("\r\n");
                            for (let line of lines) {
                                let keymaps = line.split(": ");
                                headers[keymaps[0]] = keymaps[1];
                            }
                        }

                        let results = {};
                        results.status = xhr.status;
                        results.statusText = HTTP.statusText(xhr.status);
                        if (results.statusText == "") {
                            results.statusText = xhr.statusText;
                        }
                        results.data = responseText;
                        results.headers = headers;

                        if (xhr.status >= 200 && xhr.status <= 299) {
                            results.ok = true;
                            resolve(results);
                        } else {
                            results.ok = false;
                            results.statusText = "Error";
                            reject(results);
                        }
                    }
                };
                xhr.open("POST", url, true);
                xhr.send(formdata);
            });
        }
    };
};