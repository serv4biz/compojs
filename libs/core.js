class CompoJS {
    constructor(parent, path, name, pmts = {}, lang = compojs.globals["txt_language"], data = {}) {
        compojs.globals["int_compoid"]++;
        this.stamp = UnixTime.unix();
        this.id = compojs.globals["int_compoid"];
        this.prefix = "c" + this.id + "_";
        this.parent = parent;
        this.path = path;
        this.name = name;
        this.lang = lang;
        this.pmts = pmts;
        this.childs = [];
        this.intervals = [];
        this.timeouts = [];
        this.finalizes = [];

        this.data = data;
        this.data = this.data.replaceAll("$_", this.prefix);

        let buffer = "";
        let elems = Utility.parseHTML(this.data);
        for (let elemItem of elems) {
            elemItem.setAttribute("compo", this.prefix);
            buffer += elemItem.outerHTML;
        }
        this.data = buffer;

        compojs.temps[this.prefix] = this;
    };

    static create(parent, compoPath, parameters = {}, lang = compojs.globals["txt_language"]) {
        return new Promise(function (resolve, reject) {
            let langMapping = function (jsoLang, data) {
                for (let langTag of Extract.LangTag(data)) {
                    let tag = "[-" + langTag + "-]";
                    if (JSON.exist(jsoLang, langTag)) {
                        data = data.replaceAll(tag, jsoLang[langTag]);
                    }
                }
                return data;
            };

            let paramMapping = function (parameters, data) {
                for (let keyTag of Extract.MapTag(data)) {
                    let tag = "{-" + keyTag + "-}";
                    if (JSON.exist(parameters, keyTag)) {
                        data = data.replaceAll(tag, parameters[keyTag]);
                    }
                }
                return data;
            };

            let dataMapping = function (jsoLang, parameters, compoPath, data) {
                // Language Mapping
                data = langMapping(jsoLang, data);
                // Parameter Mapping
                data = paramMapping(parameters, data);

                let names = compoPath.split("/");
                let name = names[names.length - 1];
                let compo = new CompoJS(parent, compoPath, name, parameters, lang, data);
                if (parent != null && parent != undefined) {
                    parent.childs.push(compo);
                }
                return compo;
            };

            let flow = new FlowCall();
            flow.add("lang", function () {
                CompoJS.language(lang).then(function (jsoLang) {
                    flow.call("compolang", jsoLang);
                }, function () {
                    flow.call("compolang", {});
                });
            });

            flow.add("compolang", function (jsoLang) {
                CompoJS.compoLang(compoPath, lang).then(function (jsoCompoLang) {
                    // Merge global language with component language
                    let compoLangKeys = JSON.keys(jsoCompoLang);
                    for (let keyName of compoLangKeys) {
                        jsoLang[keyName] = jsoCompoLang[keyName];
                    }
                    flow.call("create", jsoLang);
                }, function () {
                    flow.call("create", jsoLang);
                });
            });

            flow.add("create", function (jsoLang) {
                let pathName = compoPath.toLowerCase();
                if (JSON.exist(compojs.globals["obj_bundle"]["jso_compos"], pathName)) {
                    let data = compojs.globals["obj_bundle"]["jso_compos"][pathName];
                    let compo = dataMapping(jsoLang, parameters, compoPath, data);
                    resolve(compo);
                } else {
                    reject("compo not found : " + pathName);
                }
            });

            flow.call("lang");
        });
    };

    static manifest(compoPath) {
        return new Promise(function (resolve, reject) {
            let pathName = compoPath.toLowerCase();
            if (JSON.exist(compojs.globals["obj_bundle"]["jso_manifest"], pathName)) {
                let jsoManifest = compojs.globals["obj_bundle"]["jso_manifest"][pathName];
                resolve(jsoManifest);
            } else {
                reject("not found manifest"); 
            }
        });
    };

    static language(langCode) {
        return new Promise(function (resolve, reject) {
            let langname = langCode.toLowerCase();
            if (JSON.exist(compojs.globals["obj_bundle"]["jso_langs"], langname)) {
                let jsoLang = compojs.globals["obj_bundle"]["jso_langs"][langname];
                resolve(jsoLang);
            } else {
                reject("not found language");
            }
        });
    };

    static compoLang(compoPath, langCode) {
        return new Promise(function (resolve, reject) {
            let pathName = compoPath.toLowerCase();
            let langName = langCode.toLowerCase();
            if (JSON.exist(compojs.globals["obj_bundle"]["jso_compos_langs"], pathName)) {
                let jsoLangCode = compojs.globals["obj_bundle"]["jso_compos_langs"][pathName];

                if (JSON.exist(jsoLangCode, langName)) {
                    let jsoLang = jsoLangCode[langName];
                    resolve(jsoLang);
                } else {
                    reject("not found language");
                }
            } else {
                reject("not found compo");
            }
        });
    };

    language(lang = compojs.globals["txt_language"]) {
        let mythis = this;
        return new Promise(function (resolve, reject) {
            let flow = new FlowCall();
            flow.add("lang", function () {
                CompoJS.language(lang).then(function (jsoLang) {
                    flow.call("compolang", jsoLang);
                }, function () {
                    flow.call("compolang", {});
                });
            });

            flow.add("compolang", function (jsoLang) {
                CompoJS.compoLang(mythis.path, lang).then(function (jsoCompoLang) {
                    // Merge global language with component language
                    let compoLangKeys = JSON.keys(jsoCompoLang);
                    for (let keyName of compoLangKeys) {
                        jsoLang[keyName] = jsoCompoLang[keyName];
                    }
                    resolve(jsoLang);
                }, function () {
                    resolve(jsoLang);
                });
            });

            flow.call("lang");
        });
    };

    manifest() {
        let mythis = this;
        return new Promise(function (resolve, reject) {
            CompoJS.manifest(mythis.path).then(function (jsoManifest) {
                resolve(jsoManifest);
            }, reject);
        });
    };

    compo(compoPath, parameters, lang) {
        let mythis = this;
        if (lang == null || !Utility.isset(lang)) {
            lang = mythis.lang;
        }
        return CompoJS.create(this, compoPath, parameters, lang);
    };

    render(query = null) {
        if (query != null) {
            $(query).html(this.data);
        }
        return this.data;
    };

    call(name) {
        if (Utility.isset(this[name])) {
            let args = [];
            for (let i = 1; i < arguments.length; i++) {
                args.push(arguments[i]);
            }
            return this[name].apply(this, args);
        }
        return null;
    };

    get(name) {
        if (Utility.isset(this[name])) {
            return this[name];
        }
        return null;
    };

    set(name, val = null) {
        this[name] = val;
    };

    finalize(callback = function(compo){}) {
        this.finalizes.push(callback);
    };

    clearFinalize() {
        while(this.finalizes.length > 0) {
            this.finalizes.pop();
        }
    };

    delete() {
        while(this.finalizes.length > 0) {
            let callback = this.finalizes.pop();
            callback(this);
        }
        this.call("destroy");

        $id(this.prefix).remove();
        $("[compo='" + this.prefix + "']").remove();

        // Clear all childs
        for (let comItem of this.childs) {
            if (comItem != null && comItem != undefined) {
                comItem.delete();
            }
        }
        this.childs.clear();

        // Clear child in parent
        if (this.parent != null && this.parent != undefined) {
            for (let i in this.parent.childs) {
                let comItem = this.parent.childs[i];
                if (comItem != null && comItem != undefined) {
                    if (comItem.id == this.id) {
                        this.parent.childs[i] = undefined;
                        this.parent.childs.splice(i, 1);
                        break;
                    }
                }
            }
        }

        // Clear compo temp from global
        delete compojs.temps[this.prefix];

        // Clear compo object from global
        delete compojs.objects[this.prefix];

        // Clear compo resize from global
        delete compojs.resizes[this.prefix];

        // Clear all intervals
        for (let timeid of this.intervals) {
            Timer.clearInterval(timeid);
        }

        // Clear all timeouts
        for (let timeid of this.timeouts) {
            Timer.clearTimeout(timeid);
        }

        // Clear this object
        let keys = Object.keys(this);
        for (let keyname of keys) {
            delete this[keyname];
        }
    };

    interval(callback, delay) {
        let mythis = this;
        return Timer.setInterval(callback, delay, mythis);
    };

    timeout(callback, delay) {
        let mythis = this;
        return Timer.setTimeout(callback, delay, mythis);
    };

    clearInterval(timeid = 0) {
        if (timeid > 0) {
            for (let i in this.intervals) {
                if (this.intervals[i] == timeid) {
                    Timer.clearInterval(this.intervals[i]);
                    this.intervals.splice(i, 1);
                    break;
                }
            }
        } else {
            for (let timeid of this.intervals) {
                Timer.clearInterval(timeid);
            }
            this.intervals.clear();
        }
    };

    clearTimeout(timeid = 0) {
        if (timeid > 0) {
            for (let i in this.timeouts) {
                if (this.timeouts[i] == timeid) {
                    Timer.clearTimeout(this.timeouts[i]);
                    this.timeouts.splice(i, 1);
                    break;
                }
            }
        } else {
            for (let timeid of this.timeouts) {
                Timer.clearTimeout(timeid);
            }
            this.timeouts.clear();
        }
    };
};