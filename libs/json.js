JSON.exist = function (jsnobj, key) {
    return jsnobj.hasOwnProperty(key);
};

JSON.check = function (jsnobj, key) {
    return JSON.exist(jsnobj, key);
};

JSON.has = function (jsnobj, key) {
    return JSON.exist(jsnobj, key);
};

JSON.bool = function (jsnobj, key) {
    if (JSON.exist(jsnobj, key)) {
        return jsnobj[key];
    }
    return false;
};

JSON.int = function (jsnobj, key) {
    if (JSON.exist(jsnobj, key)) {
        return jsnobj[key];
    }
    return 0;
};

JSON.string = function (jsnobj, key) {
    if (JSON.exist(jsnobj, key)) {
        return jsnobj[key];
    }
    return "";
};

JSON.float = function (jsnobj, key) {
    if (JSON.exist(jsnobj, key)) {
        return jsnobj[key];
    }
    return 0.0;
};
JSON.double = JSON.float;

JSON.object = function (jsnobj, key) {
    if (JSON.exist(jsnobj, key)) {
        return jsnobj[key];
    }
    return {};
};

JSON.array = function (jsnobj, key) {
    if (JSON.exist(jsnobj, key)) {
        return jsnobj[key];
    }
    return [];
};

JSON.keys = function (jsnobj) {
    return Object.keys(jsnobj);
};

JSON.clone = function (jsoobj) {
    return JSON.parse(JSON.stringify(jsoobj));
};

JSON.copy = function (jsoobj) {
    return JSON.clone(jsoobj);
};

JSON.test = function (str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};