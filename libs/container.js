class Container {
    constructor(compoParent = null, elementId = "containers", langCompo = compojs.globals["txt_language"]) {
        this.compoParent = compoParent;
        this.elementId = elementId;
        this.lang = langCompo;
        this.compos = {};
        this.currentCompo = null;
        this.active = false;
    };

    length() {
        return Object.keys(this.compos).length;
    };

    load(pathCompo, jsoParams = {},lang = null) {
        let mythis = this;
        return new Promise(function (resolve, reject) {
            let isFound = false;

            $id(mythis.elementId).children("div").each(function (index) {
                if ($(this).attr("path") == pathCompo) {
                    $(this).show();

                    let mycompo = mythis.get(pathCompo);
                    mythis.currentCompo = mycompo;

                    if (mythis.currentCompo != null) {
                        mythis.active = true;
                        mythis.currentCompo.call("resume", jsoParams);
                    }

                    isFound = true;
                } else {
                    if ($(this).is(":visible")) {
                        $(this).hide();

                        let mycompo = mythis.get($(this).attr("path"));
                        if (mycompo != null) {
                            mycompo.call("pause");
                        }
                    }
                }
            });

            if (isFound) {
                resolve(mythis.currentCompo);
            } else {
                let comLang = mythis.lang;
                if(lang != null) {
                    comLang = lang;
                }
                CompoJS.create(mythis.compoParent, pathCompo, jsoParams, comLang).then(function (compo) {
                    mythis.compos[pathCompo] = compo;
                    mythis.currentCompo = compo;
                    mythis.active = true;

                    let pageText = "<div type='container' path='" + mythis.currentCompo.path + "' prefix='" + mythis.currentCompo.prefix + "' ></div>";
                    let pageHTML = $(pageText).html(mythis.currentCompo.render());
                    $id(mythis.elementId).append(pageHTML);
                    resolve(compo);
                },reject);
            }
        });
    };

    get(pathCompo) {
        let mythis = this;
        if (mythis.compos.hasOwnProperty(pathCompo)) {
            return mythis.compos[pathCompo];
        }
        return null;
    };

    delete(pathCompo) {
        let mythis = this;
        let compo = mythis.get(pathCompo);
        if (compo != null) {
            $("[type='container'][path='" + compo.path + "'][prefix='" + compo.prefix + "']").remove();

            if (mythis.currentCompo != null) {
                if (mythis.currentCompo.path == compo.path && mythis.currentCompo.prefix == compo.prefix) {
                    mythis.currentCompo = null;
                    mythis.active = false;
                }
            }

            compo.delete();
            delete mythis.compos[pathCompo];
        }
    };

    clear() {
        for(let pathCompo in this.compos) {
            this.delete(pathCompo);
        }
    };

    resume(pmts = {}) {
        if (this.currentCompo != null) {
            this.active = true;
            this.currentCompo.call("resume",  pmts);
        }
    };

    pause(pmts = {}) {
        if (this.currentCompo != null) {
            this.active = false;
            this.currentCompo.call("pause", pmts);
        }
    };

    setCurrent(obj) {
        this.currentCompo = obj;
    };

    getCurrent() {
        return this.currentCompo;
    };
};