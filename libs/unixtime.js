class UnixTime {
    static unix() {
        return Math.round($.now() / 1000);
    };

    static toLocalDate(unix) {
        unix = unix || UnixTime.unix();
        let slipdate = moment.unix(unix).toDate();
        let year = slipdate.getFullYear().toPadStart(4);
        let month = (slipdate.getMonth() + 1).toPadStart(2);
        let day = slipdate.getDate().toPadStart(2);
        let txtDateTime = year + "-" + month + "-" + day;
        return txtDateTime;
    };

    static toLocalTime(unix) {
        unix = unix || UnixTime.unix();
        let slipdate = moment.unix(unix).toDate();
        let hour = slipdate.getHours().toPadStart(2);
        let minute = slipdate.getMinutes().toPadStart(2);
        let second = slipdate.getSeconds().toPadStart(2);
        let txtDateTime = hour + ":" + minute + ":" + second;
        return txtDateTime;
    };

    static toLocalString(unix) {
        return UnixTime.toLocalDate(unix) + " " + UnixTime.toLocalTime(unix);
    };

    static toUTCDate(unix) {
        unix = unix || UnixTime.unix();
        let slipdate = moment.unix(unix).toDate();
        let year = slipdate.getUTCFullYear().toPadStart(4);
        let month = (slipdate.getUTCMonth() + 1).toPadStart( 2);
        let day = slipdate.getUTCDate().toPadStart(2);
        let txtDateTime = year + "-" + month + "-" + day;
        return txtDateTime;
    };

    static toUTCTime(unix) {
        unix = unix || UnixTime.unix();
        let slipdate = moment.unix(unix).toDate();
        let hour = slipdate.getUTCHours().toPadStart(2);
        let minute = slipdate.getUTCMinutes().toPadStart(2);
        let second = slipdate.getUTCSeconds().toPadStart(2);
        let txtDateTime = hour + ":" + minute + ":" + second;
        return txtDateTime;
    };

    static toUTCString(unix) {
        return UnixTime.toUTCDate(unix) + " " + UnixTime.toUTCTime(unix);
    };
};