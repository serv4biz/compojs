class Utility {
  static isset(variable) {
    return (typeof variable !== 'undefined');
  };

  static random(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  static encodeHTMLEntities(val) {
    var textArea = document.createElement('textarea');
    textArea.innerText = val;
    return textArea.innerHTML;
  };

  static decodeHTMLEntities(val) {
    let textArea = document.createElement('textarea');
    textArea.innerHTML = val;
    return textArea.value;
  };

  static parseHTML(html) {
    let elems = document.createElement("div");
    elems.innerHTML = Utility.decodeHTMLEntities(html);
    return elems.children;
  };
};