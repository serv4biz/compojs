class Cookie {
    static set(name, value, unix = 86400) {
        let maxage = "Max-Age=" + unix;
        document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + ";" + maxage + ";path=/";
        return Cookie.get(name) != "";
    };

    static get(name) {
        let cname = name + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(cname) == 0) {
                return c.substring(cname.length, c.length);
            }
        }
        return "";
    };

    static delete(name) {
        document.cookie = encodeURIComponent(name) + "=; Max-Age=-1; path=/;";
        return Cookie.get(name) == "";
    };
};