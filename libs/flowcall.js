class FlowCallItem {
    name = "";
    callback = function(pmts){};

    constructor(callName,callBack = {}) {
        this.name = callName.toLowerCase();
        this.callback = callBack;
    };

    call(pmts = {}){
        this.callback(pmts);
    };

    setName(name) {
        this.name = name;
    };

    getName() {
        return this.name;
    };

    setCallback(cb) {
        this.callback = cb;
    };

    getCallback() {
        return this.callback;
    };
};

class FlowCall {
    objMethods = {};

    constructor() {};

    clear() {
        this.objMethods.clear();
    };

    delete(callName) {
        this.objMethods.delete(callName);
    };

    add(callName,callBack) {
        let name = callName.toLowerCase();
        this.objMethods[name] = new FlowCallItem(name,callBack);
    };

    call(callName = null,pmts = {}) {
        let name = callName.toLowerCase();
        this.objMethods[name].call(pmts);
    };
};