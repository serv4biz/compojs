class BigUtil {
    static toNumberString (val, decimal = 0) {
        val = BigInt(val);
        let buffs = val.toString();
    
        let result = "";
        let count = 0;
        for (let i = (buffs.length - 1); i >= 0; i--) {
            let ch = buffs.charAt(i);
            result = ch + result;
    
            count++;
            if (count == decimal) {
                result = "." + result;
            }
        }
        result = result.replace(/(^\.+)/g, "");
        result = result.replace(/(\.+$)/g, "");
        let dif = decimal - buffs.length;
        if (dif >= 0) {
            for (let i = 0; i < dif; i++) {
                result = "0" + result;
            }
            result = "0." + result;
        }
        return result;
    };

    static toNumber (val, decimal = 0) {
        return Number.parseFloat(BigUtil.toNumberString(val,decimal));
    };

    static fromNumberString (val, decimal = 0) {
        let tmp = val;
        if(!tmp.includes(".")) {
            tmp += "."
        }
        let tmps = tmp.split(".",2);
        let first = tmps[0];
        first = first.replace(/(^0+)/g, "");
    
        let second = tmps[1];
        let diff = decimal - second.length;
        for(let i=0;i<diff;i++) {
            second += "0";
        }
        if(diff < 0) {
            let trim = "";
            for(let i=0;i<decimal;i++) {
                trim += second.charAt(i);
            }
            second = trim;
        }
        return BigInt(first.trim() + second.trim());
    };
    
    static fromNumber (val, decimal = 0) {
        return BigInt(BigUtil.fromNumberString(String(Number.parseFloat(val).toFixed(decimal)),decimal));
    };
}