class Base64 {
    static encode(val) {
        return window.btoa(val);
    };

    static decode(val) {
        return window.atob(val);
    };

    static encodeUTF8(val) {
        return window.btoa(unescape(encodeURIComponent(val)));
    };

    static decodeUTF8(val) {
        return decodeURIComponent(escape(window.atob(val)));
    };
};