
class Extract {
    static LangTag(data) {
        const NULL = 0;
        const BRACKET = 1;

        let list = [];
        let tag = "";
        let level = 0;

        let mode = NULL;
        for (let i = 0; i < data.length; i++) {
            let ch = data.charAt(i);
            switch (mode) {
                case NULL:
                    if (ch == '[' && data.substr(i, 2) == "[-") {
                        if (i > 0 && data.charAt(i - 1) == '\\') {
                            break;
                        }
                        tag += ch;
                        mode = BRACKET;
                        level++;
                    }
                    break;
                case BRACKET:
                    tag += ch;
                    if (ch == '[' && data.substr(i,  2) == "[-" && data.charAt(i - 1) != '\\') {
                        level++;
                    } else if (ch == '-' && data.substr(i, 2) == "-]" && data.charAt(i - 1) != '\\') {
                        level--;
                    }

                    if (level == 0) {
                        tag += data.charAt(i + 1);

                        mode = NULL;
                        tag = tag.replace(/^(\[-)/, '');
                        tag = tag.replace(/(-\])$/, '');
                        list.push(tag);
                        tag = "";
                    }
                    break;
            }
        }

        return list;
    };

    static MapTag(data) {
        const NULL = 0;
        const BRACKET = 1;

        let list = [];
        let tag = "";
        let level = 0;

        let mode = NULL;
        for (let i = 0; i < data.length; i++) {
            let ch = data.charAt(i);
            switch (mode) {
                case NULL:
                    if (ch == '{' && data.substr(i, 2) == "{-") {
                        if (i > 0 && data.charAt(i - 1) == '\\') {
                            break;
                        }
                        tag += ch;
                        mode = BRACKET;
                        level++;
                    }
                    break;
                case BRACKET:
                    tag += ch;
                    if (ch == '{' && data.substr(i,  2) == "{-" && data.charAt(i - 1) != '\\') {
                        level++;
                    } else if (ch == '-' && data.substr(i, 2) == "-}" && data.charAt(i - 1) != '\\') {
                        level--;
                    }

                    if (level == 0) {
                        tag += data.charAt(i + 1);

                        mode = NULL;
                        tag = tag.replace(/^(\{-)/, '');
                        tag = tag.replace(/(-\})$/, '');
                        list.push(tag);
                        tag = "";
                    }
                    break;
            }
        }

        return list;
    };
};