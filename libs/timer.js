class Timer {
    static clear() {
        Timer.clearInterval();
        Timer.clearTimeout();
    };

    static setInterval(callback, delay = 1000, compo = null) {
        let timeid = window.setInterval(function () {
            if (compo != null) {
                let length = $("[compo='" + compo.prefix + "']:first").length;
                if (length <= 0) {
                    for(let curid of compo.intervals) {
                        Timer.clearInterval(curid);
                    }
                    return false;
                }
            }

            callback();
        }, delay);

        compojs.globals["jsa_interval"].push(timeid);
        if (compo != null) {
            compo.intervals.push(timeid);
        }
        return timeid;
    };

    static clearInterval(timeid = 0) {
        if (timeid > 0) {
            window.clearInterval(timeid);

            // Find time id in global
            for (let i in compojs.globals["jsa_interval"]) {
                if (compojs.globals["jsa_interval"][i] == timeid) {
                    compojs.globals["jsa_interval"].slice(i, 1);
                    break;
                }
            }
        } else {
            while (compojs.globals["jsa_interval"].length > 0) {
                let timeid = compojs.globals["jsa_interval"].shift();
                window.clearInterval(timeid);
            }
        }
    };

    static setTimeout(callback, delay = 1000, compo = null) {
        let timeid = window.setTimeout(function () {
            if (compo != null) {
                let length = $("[compo='" + compo.prefix + "']:first").length;
                if (length <= 0) {
                    for (let curid of compo.timeouts) {
                        Timer.clearTimeout(curid);
                    }
                    return false;
                }
            }

            callback();
        }, delay);

        compojs.globals["jsa_timeout"].push(timeid);
        if (compo != null) {
            compo.timeouts.push(timeid);
        }
        return timeid;
    };

    static clearTimeout(timeid = 0) {
        if (timeid > 0) {
            window.clearTimeout(timeid);

            // Find time id in global
            for (let i in compojs.globals["jsa_timeout"]) {
                if (compojs.globals["jsa_timeout"][i] == timeid) {
                    compojs.globals["jsa_timeout"].slice(i, 1);
                    break;
                }
            }
        } else {
            while (compojs.globals["jsa_timeout"].length > 0) {
                let timeid = compojs.globals["jsa_timeout"].shift();
                window.clearTimeout(timeid);
            }
        }
    };
};