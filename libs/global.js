// Global Variable
compojs.globals = {
    "int_compoid": 0,
    "obj_cache": {},
    "obj_bundle": {},
    "txt_language": "en",
    "jsa_interval": [],
    "jsa_timeout": []
};

compojs.temps = {};
compojs.objects = {};
compojs.resizes = {};
compojs.ready = function(){};

// Local Database
compojs.storages = {
    locals : localforage.createInstance({
        name: "compojs"
    }),
    caches : localforage.createInstance({
        name: "caches"
    })
};

// Change global language to default setting
compojs.globals["txt_language"] = compojs.configs["txt_language"];