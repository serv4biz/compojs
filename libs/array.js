Array.prototype.clear = function () {
    while (this.length > 0) {
        this.shift();
    }
};

Array.prototype.delete = function (index = 0) {
    this.splice(index, 1);
};

Array.prototype.remove = function (index = 0) {
    this.splice(index, 1);
};