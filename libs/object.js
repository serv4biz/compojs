Object.defineProperty(Object.prototype, 'clear', {
    value: function () {
        for (key in this) {
            delete this[key];
        }
    },
    writable:true
});

Object.defineProperty(Object.prototype, 'delete', {
    value: function (key) {
        delete this[key];
    },
    writable:true
});

Object.defineProperty(Object.prototype, 'remove', {
    value: function (key) {
        delete this[key];
    },
    writable:true
});