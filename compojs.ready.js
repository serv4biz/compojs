/* Auto config base url */
if(JSON.exist(compojs.configs,"bln_auto_baseurl")) {
    if(compojs.configs["bln_auto_baseurl"]) {
        if(location.protocol.replace(":","") != "file") {
            compojs.configs["txt_protocol"] = location.protocol.replace(":","");
            compojs.configs["txt_host"] = location.hostname;
            compojs.configs["int_port"] = location.port;
            compojs.configs["txt_baseurl"] = compojs.configs["txt_protocol"] + "://" + compojs.configs["txt_host"];
            
            if(compojs.configs["int_port"] == '') {
                compojs.configs["int_port"] = 80;
            }
            
            if(compojs.configs["int_port"] != 80) {
                compojs.configs["txt_baseurl"] += ":" + compojs.configs["int_port"]
            }
        }
    }
}
compojs.globals["obj_bundle"] = {/*bundle*/};

/* Garbage Collection Cycle */
window.setInterval(function(){
    let stamp = UnixTime.unix();
    let keys = Object.keys(compojs.temps);
    for(let i=0;i<keys.length;i++) {
        let prefix = keys[i];
        let compo = compojs.temps[prefix];
        if(typeof compo === 'object') {
            let diff = stamp - compo.stamp;
            if(diff >= compojs.configs["int_gc_cycle"]) {
                compo.delete();
                delete compojs.temps[prefix];
            }
        } else {
            delete compojs.temps[prefix];
        }
    }

    keys = Object.keys(compojs.objects);
    for(let i=0;i<keys.length;i++) {
        let prefix = keys[i];
        let compo = compojs.objects[prefix];
        if(typeof compo === 'object') {
            let diff = stamp - compo.stamp;
            if(diff >= compojs.configs["int_gc_cycle"]) {
                let length = $("[compo='" + compo.prefix + "']:first").length;
                if (length <= 0) {
                    compo.delete();
                    delete compojs.objects[prefix];
                }
            }
        } else {
            delete compojs.objects[prefix];
        }
    }
},compojs.configs["int_gc_cycle"] * 1000);

window.onresize = function(e){
    keys = Object.keys(compojs.resizes);
    for(let i=0;i<keys.length;i++) {
        let prefix = keys[i];
        let compo = compojs.resizes[prefix];
        if(typeof compo === 'object') {
            compo.call("resize",e);
        } else {
            delete compojs.resizes[prefix];
        }
    }
};

compojs.ready();