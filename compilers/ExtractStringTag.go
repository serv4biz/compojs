package compilers

import "strings"

func ExtractStringTag(data string) []string {
	const (
		NULL         int = iota
		DOUBLE_QUOTE     //	Double quote
		SINGLE_QUOTE     // Single quote
		BACK_QUOTE       // backquote / backtick
	)
	list := make([]string, 0)

	tag := ""
	level := 0

	mode := NULL
	buffers := []rune(data)
	for i, ch := range buffers {
		switch mode {
		case NULL:
			if ch == '"' || ch == '\'' || ch == '`' {
				if i > 0 && buffers[i-1] == '\\' {
					break
				}

				if ch == '"' {
					mode = DOUBLE_QUOTE
				} else if ch == '\'' {
					mode = SINGLE_QUOTE
				} else if ch == '`' {
					mode = BACK_QUOTE
				}

				level++
				tag += string(ch)
			}
		case DOUBLE_QUOTE:
			tag += string(ch)
			if ch == '"' && buffers[i-1] != '\\' {
				level--
			}

			if level == 0 {
				mode = NULL
				list = append(list, strings.Trim(tag, "\""))
				tag = ""
			}
		case SINGLE_QUOTE:
			tag += string(ch)
			if ch == '\'' && buffers[i-1] != '\\' {
				level--
			}

			if level == 0 {
				mode = NULL
				list = append(list, tag, strings.Trim(tag, "'"))
				tag = ""
			}
		case BACK_QUOTE:
			tag += string(ch)
			if ch == '`' && buffers[i-1] != '\\' {
				level--
			}

			if level == 0 {
				mode = NULL
				list = append(list, strings.Trim(tag, "`"))
				tag = ""
			}
		}
	}

	return list
}
