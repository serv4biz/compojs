package compilers

import (
	"errors"
	"strings"

	"gitlab.com/serv4biz/gfp/minify"
)

// Minify is compress script code
func Minify(exttype string, buffer string) (string, error) {
	if strings.ToLower(exttype) == "css" {
		return minify.CSS(buffer)
	} else if strings.ToLower(exttype) == "js" {
		return minify.JS(buffer)
	} else if strings.ToLower(exttype) == "html" {
		return minify.HTML(buffer)
	} else if strings.ToLower(exttype) == "xml" {
		return minify.XML(buffer)
	} else if strings.ToLower(exttype) == "svg" {
		return minify.SVG(buffer)
	} else if strings.ToLower(exttype) == "json" {
		return minify.JSON(buffer)
	}
	return buffer, errors.New("minify format is not support")
}
