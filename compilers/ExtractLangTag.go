package compilers

import (
	"strings"
)

// Extract language tag
func ExtractLangTag(data string) []string {
	const (
		NULL    int = iota
		BRACKET     // square bracket
	)

	list := make([]string, 0)
	tag := ""
	level := 0

	mode := NULL
	buffers := []rune(data)
	for i, ch := range buffers {
		switch mode {
		case NULL:
			if ch == '[' && string(buffers[i:i+2]) == "[-" {
				if i > 0 && buffers[i-1] == '\\' {
					break
				}
				tag += string(ch)
				mode = BRACKET
				level++
			}
		case BRACKET:
			tag += string(ch)
			if ch == '[' && string(buffers[i:i+2]) == "[-" && buffers[i-1] != '\\' {
				level++
			} else if ch == '-' && string(buffers[i:i+2]) == "-]" && buffers[i-1] != '\\' {
				level--
			}

			if level == 0 {
				tag += string(buffers[i+1])

				mode = NULL
				list = append(list, strings.TrimSuffix(strings.TrimPrefix(tag, "[-"), "-]"))
				tag = ""
			}
		}
	}
	return list
}
