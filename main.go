package main

import (
	"bytes"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"strings"

	"gitlab.com/serv4biz/compojs/compilers"
	"gitlab.com/serv4biz/gfp/errs"
	"gitlab.com/serv4biz/gfp/logs"

	"embed"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
)

//go:embed compojs.ready.js
var efs embed.FS

var AppDir string = ""
var LanguagePath string = ""

var jsoBundleLangs *jsons.Object = jsons.ObjectNew(0)
var jsoBundleCompos *jsons.Object = jsons.ObjectNew(0)
var jsoBundleComposLangs *jsons.Object = jsons.ObjectNew(0)
var jsoBundleManifest *jsons.Object = jsons.ObjectNew(0)
var jsoBundle *jsons.Object = jsons.ObjectNew(0)

// CompileCompos is compile compos all and add to jsoBundleCompos object
func CompileCompos(pathfile string) {
	filedirs, err := files.ScanDir(pathfile)
	logs.Panic(err, true)

	for _, fileName := range filedirs {
		pathFile := fmt.Sprint(pathfile, "/", fileName)

		if files.IsDir(pathFile) && fileName != LanguagePath {
			pathLang := fmt.Sprint(pathFile, "/", LanguagePath)
			pathStyle := fmt.Sprint(pathFile, "/style.css")
			pathView := fmt.Sprint(pathFile, "/view.html")
			pathCode := fmt.Sprint(pathFile, "/code.js")
			pathManifest := fmt.Sprint(pathFile, "/manifest.json")
			pathobject := fmt.Sprint(pathFile, "/compo.obj")
			isCompo := false

			if jsoBundleLangs.Length() > 0 {
				files.MakeDir(pathLang, fs.ModePerm)
			}

			buff := new(bytes.Buffer)
			if files.ExistFile(pathStyle) {
				b, err := files.ReadFile(pathStyle)
				logs.Panic(err, true)

				mini, err := compilers.Minify("css", string(b))
				logs.Panic(err, true)

				buff.WriteString("<style>")
				buff.WriteString(mini)
				buff.WriteString("</style>")
				isCompo = true
			}

			if files.ExistFile(pathView) {
				b, err := files.ReadFile(pathView)
				logs.Panic(err, true)

				mini, err := compilers.Minify("html", string(b))
				logs.Panic(err, true)

				buff.WriteString(mini)
				isCompo = true

				// Generate Language
				jsoBundleLangs.EachObject(func(langName string, jsoLang *jsons.Object) error {
					jsoCompoLang := jsons.ObjectNew(0)
					langFile := fmt.Sprint(pathLang, "/", langName, ".json")
					if files.ExistFile(langFile) {
						jsoCompoLang, err = jsons.ObjectFromFile(langFile)
						logs.Panic(err, true)
					}

					for _, val := range compilers.ExtractLangTag(mini) {
						if !jsoCompoLang.Check(val) {
							if jsoLang.Check(val) {
								jsoCompoLang.PutString(val, jsoLang.String(val))
							} else {
								jsoCompoLang.PutString(val, val)
							}
						}
					}

					jsoCompoLang.EachString(func(key, value string) error {
						if !jsoLang.Check(key) {
							jsoLang.PutString(key, value)
						}
						return nil
					})
					jsoCompoLang.ToFile(langFile)
					return nil
				})
			}

			if files.ExistFile(pathCode) {
				b, err := files.ReadFile(pathCode)
				logs.Panic(err, true)

				mini, err := compilers.Minify("js", string(b))
				logs.Panic(err, true)

				// Generate Language
				jsoBundleLangs.EachObject(func(langName string, jsoLang *jsons.Object) error {
					jsoCompoLang := jsons.ObjectNew(0)
					langFile := fmt.Sprint(pathLang, "/", langName, ".json")
					if files.ExistFile(langFile) {
						jsoCompoLang, err = jsons.ObjectFromFile(langFile)
						logs.Panic(err, true)
					}

					for _, str := range compilers.ExtractStringTag(mini) {
						for _, val := range compilers.ExtractLangTag(str) {
							if !jsoCompoLang.Check(val) {
								if jsoLang.Check(val) {
									jsoCompoLang.PutString(val, jsoLang.String(val))
								} else {
									jsoCompoLang.PutString(val, val)
								}
							}
						}
					}

					jsoCompoLang.EachString(func(key, value string) error {
						if !jsoLang.Check(key) {
							jsoLang.PutString(key, value)
						}
						return nil
					})

					jsoCompoLang.ToFile(langFile)
					return nil
				})

				buff.WriteString("<script>")
				buff.WriteString(`compojs.objects["$_"] = compojs.temps["$_"];`)
				buff.WriteString(`var $_this = compojs.temps["$_"];`)
				buff.WriteString("var $_parent = $_this.parent;")
				buff.WriteString(mini)
				buff.WriteString(`if(Utility.isset($_this["resize"])){compojs.resizes["$_"] = compojs.objects["$_"];}`)
				buff.WriteString(`$_this.call("ready",$_this.pmts);`)
				buff.WriteString(`$_this.call("resume",$_this.pmts);`)
				buff.WriteString(`delete compojs.temps["$_"];`)
				buff.WriteString("</script>")
				isCompo = true
			} else {
				buff.WriteString("<script>")
				buff.WriteString(`compojs.objects["$_"] = compojs.temps["$_"];`)
				buff.WriteString(`delete compojs.temps["$_"];`)
				buff.WriteString("</script>")
				isCompo = true
			}

			if isCompo {
				min, err := compilers.Minify("html", buff.String())
				logs.Panic(err, true)

				files.WriteFile(pathobject, []byte(min), fs.ModePerm)
				fmt.Println(pathFile)

				// Add to bundle
				basePath := fmt.Sprint(AppDir, "/compos")
				bundlePath := strings.ToLower(strings.Trim(strings.ReplaceAll(pathFile, basePath, ""), "/"))
				jsoBundleCompos.SetString(bundlePath, buff.String())

				// Bundle manifest
				if files.ExistFile(pathManifest) {
					jsoManifest, err := jsons.ObjectFromFile(pathManifest)
					logs.Panic(err, true)
					jsoBundleManifest.PutObject(bundlePath, jsoManifest)
				}

				// Bundle language
				jsoEmptyLangs := jsons.ObjectNew(0)
				jsoBundleLangs.EachObject(func(key string, value *jsons.Object) error {
					jsoEmptyLangs.PutObject(key, jsons.ObjectNew(0))
					return nil
				})
				jsoBundleComposLangs.PutObject(bundlePath, jsoEmptyLangs)

				if files.ExistFile(pathLang) {
					files, err := files.ScanDir(pathLang)
					logs.Panic(err, true)

					jsoLangs := jsons.ObjectNew(0)
					for _, name := range files {
						if strings.HasSuffix(strings.ToLower(name), ".json") {
							pathitem := fmt.Sprint(pathLang, "/", name)

							// Add to bundle
							langcode := strings.ToLower(strings.ReplaceAll(strings.ToLower(name), ".json", ""))
							buff, err := jsons.ObjectFromFile(pathitem)
							logs.Panic(err, true)

							jsoLangs.PutObject(langcode, buff)
						}
					}
					jsoBundleComposLangs.PutObject(bundlePath, jsoLangs)
				}
			}
			CompileCompos(pathFile)
		}
	}
}

// CompileLangs is compile all langs all and add to jsoBundleLangs object
func CompileLangs(pathfile string) {
	fileNames, err := files.ScanDir(pathfile)
	logs.Panic(err, true)

	for _, name := range fileNames {
		pathitem := fmt.Sprint(pathfile, "/", name)

		// Add to bundle
		langName := strings.ToLower(strings.ReplaceAll(strings.ToLower(name), ".json", ""))
		jsoItem, err := jsons.ObjectFromFile(pathitem)
		logs.Panic(err, true)

		jsoBundleLangs.PutObject(langName, jsoItem)
	}
}

// CompileJs is minify compress javascript
func CompileJs(buffer *bytes.Buffer, pathFile string) {
	b, err := files.ReadFile(pathFile)
	logs.Panic(err, true)

	mini, err := compilers.Minify("js", string(b))
	logs.Panic(err, true)

	buffer.WriteString(mini)
}

// Add source from path8
func AddSource(jsaSource *jsons.Array, pathSource string) {
	pathFile := AppDir + "/" + pathSource
	if files.ExistFile(pathFile) {
		if files.IsDir(pathFile) {
			strFiles, err := files.ScanDir(pathFile)
			logs.Panic(err, true)

			for _, strNewItem := range strFiles {
				pathNewSource := pathSource + "/" + strNewItem
				AddSource(jsaSource, pathNewSource)
			}
		} else {
			jsaSource.PutString(pathSource)
		}
	}
}

func main() {
	Args := os.Args
	ProjectName := "CompoJS"
	ProjectVersion := "1.0.1"
	CompanyName := "SERV4BIZ CO.,LTD."

	isCompileLib := false
	if len(Args) > 1 {
		for _, value := range Args {
			if strings.ToLower(value) == "lib" {
				isCompileLib = true
			}
		}
	}
	var err error = nil
	AppDir, err = files.GetAppDir()
	logs.Panic(err, true)

	pathConfig := fmt.Sprint(AppDir, "/compojs.json")
	jsoConfig, err := jsons.ObjectFromFile(pathConfig)
	logs.Panic(err, true)

	var ok bool
	LanguagePath, ok = jsoConfig.GetString("txt_language_path")
	if !ok {
		logs.Panic(errors.New("not found txt_language_path in json object"), true)
	}

	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println(fmt.Sprint(ProjectName, " Version ", ProjectVersion))
	fmt.Println(fmt.Sprint("Copyright © 2020 ", CompanyName, " All Rights Reserved."))
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println(fmt.Sprint("Directory : ", AppDir))
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("Begin Components Compiling")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("")

	pathdir := fmt.Sprint(AppDir, "/", LanguagePath)
	fileNames, err := files.ScanDir(pathdir)
	logs.Panic(err, true)
	for _, fileName := range fileNames {
		pathFile := fmt.Sprint(pathdir, "/", fileName)

		jsoLang, err := jsons.ObjectFromFile(pathFile)
		logs.Panic(err, true)

		langCode := strings.TrimSuffix(fileName, ".json")
		jsoBundleLangs.PutObject(langCode, jsoLang)
	}
	CompileLangs(pathdir)

	pathdir = fmt.Sprint(AppDir, "/compos")
	CompileCompos(pathdir)

	fmt.Println("")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")

	jsoBundle.PutObject("jso_langs", jsoBundleLangs)
	jsoBundle.PutObject("jso_compos", jsoBundleCompos)
	jsoBundle.PutObject("jso_compos_langs", jsoBundleComposLangs)
	jsoBundle.PutObject("jso_manifest", jsoBundleManifest)
	txtBundle, err := jsoBundle.ToString()
	logs.Panic(err, true)

	// Load bundle json file
	readyData, err := efs.ReadFile("compojs.ready.js")
	logs.Panic(err, true)

	readyCode := strings.Replace(string(readyData), "{/*bundle*/}", txtBundle, 1)
	mini, err := compilers.Minify("js", strings.TrimSpace(readyCode))
	logs.Panic(err, true)

	pathfile := fmt.Sprint(AppDir, "/compojs.app.js")

	mini = strings.ReplaceAll(mini, "\\u003c", "<")
	mini = strings.ReplaceAll(mini, "\\u003e", ">")

	_, err = files.WriteFile(pathfile, []byte(mini), fs.ModePerm)
	logs.Panic(err, true)

	if isCompileLib {
		buffer := new(bytes.Buffer)

		// Compile vendor libraries
		jsaVenders := jsons.ArrayNew(0)
		jsaVenders.AddString("assets/js/lib/localforage.min.js")
		jsaVenders.AddString("assets/js/lib/tocca.min.js")
		jsaVenders.AddString("assets/js/lib/jquery-3.7.1.min.js")
		jsaVenders.AddString("assets/js/lib/jquery.tocca.js")
		jsaVenders.AddString("assets/js/lib/jquery.extra.js")
		jsaVenders.AddString("assets/js/lib/screenfull.min.js")
		jsaVenders.AddString("assets/js/lib/sha256.min.js")
		jsaVenders.AddString("assets/js/lib/md5.min.js")
		jsaVenders.AddString("assets/js/lib/clipboard-polyfill.js")
		jsaVenders.AddString("assets/js/lib/moment.min.js")

		for i := 0; i < jsaVenders.Length(); i++ {
			pathLibFile := fmt.Sprint(AppDir, "/", jsaVenders.GetString(i))
			CompileJs(buffer, pathLibFile)
			buffer.WriteString("\n")
		}
		// End of compile venders

		pathLib := fmt.Sprint(AppDir, "/libs")
		filedirs, err := files.ScanDir(pathLib)
		logs.Panic(err, true)

		for _, fileName := range filedirs {
			if strings.HasSuffix(strings.ToLower(fileName), ".js") {
				pathjs := fmt.Sprint(pathLib, "/", fileName)
				CompileJs(buffer, pathjs)
				buffer.WriteString("\n")
			}
		}

		pathfile := fmt.Sprint(AppDir, "/compojs.lib.js")
		_, err = files.WriteFile(pathfile, buffer.Bytes(), fs.ModePerm)
		logs.Panic(err, true)
	}

	// Export sources.json
	txtVersion, ok := jsoConfig.GetString("txt_version")
	if !ok {
		logs.Panic(errors.New("no txt_version key in json object"), true)
	}

	jsaSource := jsons.ArrayNew(0)

	jsaListing, ok := jsoConfig.GetArray("jsa_source")
	if !ok {
		logs.Panic(errors.New("no jsa_source key in json object"), true)
	}

	jsaListing.EachString(func(index int, value string) error {
		AddSource(jsaSource, value)
		return nil
	})

	jsaResult := jsons.ArrayNew(0)
	// Delist
	jsaDelist, ok := jsoConfig.GetArray("jsa_delist")
	if !ok {
		logs.Panic(errors.New("no jsa_delist key in json object"), true)
	}

	jsaSource.EachString(func(index int, sourceItem string) error {
		blnPass := true
		jsaDelist.EachString(func(index int, delistItem string) error {
			if strings.HasPrefix(sourceItem, jsons.AnyString(delistItem)) {
				blnPass = false
				return errs.ErrBreak
			}
			return nil
		})

		if blnPass {
			jsaResult.AddString(sourceItem)
		}
		return nil
	})

	jsoResult := jsons.ObjectNew(0)
	jsoResult.SetString("txt_version", txtVersion)
	jsoResult.SetArray("jsa_source", jsaResult)

	pathSources := AppDir + "/sources.json"
	_, err = jsoResult.ToFile(pathSources)
	logs.Panic(err, true)

	// Export lang all
	jsoBundleLangs.EachObject(func(key string, value *jsons.Object) error {
		pathFile := fmt.Sprint(AppDir, "/", LanguagePath, "/", key, ".json")
		_, err := value.ToFile(pathFile)
		logs.Panic(err, true)
		return nil
	})

	fmt.Println(fmt.Sprint(ProjectName, " Finished"))
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("")
}
