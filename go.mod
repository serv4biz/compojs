module gitlab.com/serv4biz/compojs

go 1.23

replace gitlab.com/serv4biz/compojs => ./

require gitlab.com/serv4biz/gfp v1.2.6
