#!/bin/bash

# cloudflare
CLOUDFLARE_KEY="d523918e33046e625a8f5b1896a30d2a88f16"
CLOUDFLARE_EMAIL="brutal99@gmail.com"
CLOUDFLARE_ZONE="0ee8f34de269f212401609dc7f37d235"

# clear proxy cache files
# s4blab.com
curl --location --request POST "https://api.cloudflare.com/client/v4/zones/$CLOUDFLARE_ZONE/purge_cache" \
--header "X-Auth-Key: $CLOUDFLARE_KEY" \
--header "X-Auth-Email: $CLOUDFLARE_EMAIL" \
--header 'Content-Type: application/json' \
--data '{"purge_everything":true}'

# set proxy to development mode
# s4blab.com
curl --location --request PATCH "https://api.cloudflare.com/client/v4/zones/$CLOUDFLARE_ZONE/settings/development_mode" \
--header "X-Auth-Key: $CLOUDFLARE_KEY" \
--header "X-Auth-Email: $CLOUDFLARE_EMAIL" \
--header 'Content-Type: application/json' \
--data-raw '{"value":"on"}'

RUNCOMPOJS="compojs.$1.$2"

clear
./build.sh
./$RUNCOMPOJS lib