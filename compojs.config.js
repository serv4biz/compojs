var compojs = {
    configs: {
        "txt_version": "1.0.0",
        "txt_protocol": "http",
        "txt_host": "localhost",
        "int_port": 5500,
        "txt_baseurl": "http://localhost:5500",

        "is_devmode": true,
        "txt_language": "en",
        "txt_language_path": "languages",
        "int_gc_cycle": 600,
        "bln_auto_baseurl": true
    }
};